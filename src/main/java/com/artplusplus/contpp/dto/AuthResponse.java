package com.artplusplus.contpp.dto;

import com.artplusplus.contpp.model.Grupo;

public record AuthResponse(Grupo grupo, Long userId, String username) { }
